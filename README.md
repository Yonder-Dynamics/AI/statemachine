<img src="assets/logo-without-text.png" width="240">

# Yonder Dynamics

# State-Machine

## Summary
This repo contains the controls for the processes of an autonomous to complete the task of locating a tennis ball, as implemented by the originating authors. The State-Machine includes the ability to interact with a trained neural network trained to detect interest points and use likelihoods to aid in its task, as well as implementations of tests to optimize it. As a result, the detected points can thus be used for image recognition and pathing processes. 

## Dependencies
* [OpenCV](https://opencv.org/) python >= 3.4
* [numpy](http://www.numpy.org/) >= 1.16.2



## Contents

Assets: Assets needed for documentation

bin: Contains simulation for TSP testing

design: Contains the design models towards the implementation of pathing algorithm

old: Contains previous implementations of the pathing algorithms

src: Contains new implementation of pathing algorithm in accordance to the model shown in bin and design



## Additional Notes
* N/A



