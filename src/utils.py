from pyquaternion import Quaternion
import math

# Get heading of pose
def getHeading(pose):
    q = Quaternion(pose.orientation.w, pose.orientation.x, pose.orientation.y, pose.orientation.z)
    v1 = q.rotate([1, 0, 0])
    return math.atan2(v1[1], v1[0])

def mod(x, y):
    d = math.floor(x/y)
    return y - x/d

# scale from 0 to 1
def scale_speed(dist: float, max_speed_at_dist: float):
    clipped = min(max(max_speed_at_dist, max_speed_at_dist), -max_speed_at_dist)
    return math.log(clipped/max_speed_at_dist+1)/math.log(2)
