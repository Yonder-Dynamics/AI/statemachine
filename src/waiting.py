import signals
import rediskeys
import settings

def waiting():
    destination = input('Enter Destination: ')
    settings.gvars.db.set(rediskeys.GPS_GOAL, destination)
    start = settings.gvars.db.get(rediskeys.GPS)
    settings.gvars.db.set(rediskeys.GPS_START, start)
    return signals.RECIEVED_NEXT
