import math
import signals
import rediskeys 
import numpy as np
# from main import settings.gvars
import settings
from haversine import haversine 
from utils import getHeading, scale_speed

def update_gps():
    # retrieve the current gps location from redis
    return setttings.gvars.db.get("SENSORS:GPS")

def update_pose():
    # retrieve the pose from redis
    pose = setttings.gvars.db.get("SENSORS:POSE")
    return pose.position, getHeading(pose)

def get_haversine(gps=None, goal=None):
    if not gps:
      gps: Tuple[float, float] = update_gps()
    if not goal:
      goal: Tuple[float, float] = setttings.gvars.db.get("AT:GPS_GOAL")

    # return the haversine distance
    return haversine(gps, goal, unit='mi')

def get_bearing(pointA, pointB):
    """
    Calculates the bearing between two points.
    The formulae used is the following:
        θ = atan2(sin(Δlong).cos(lat2),
                  cos(lat1).sin(lat2) − sin(lat1).cos(lat2).cos(Δlong))
    :Returns:
      The bearing in degrees
    :Returns Type:
      float
    """
    if (type(pointA) != tuple) or (type(pointB) != tuple):
        raise TypeError("Only tuples are supported as arguments")

    lat1 = math.radians(pointA[0])
    lat2 = math.radians(pointB[0])

    diffLong = math.radians(pointB[1] - pointA[1])

    x = math.sin(diffLong) * math.cos(lat2)
    y = math.cos(lat1) * math.sin(lat2) - (math.sin(lat1)
            * math.cos(lat2) * math.cos(diffLong))

    initial_bearing = math.atan2(x, y)

    # Now we have the initial bearing but math.atan2 return values
    # from -180° to + 180° which is not what we want for a compass bearing
    # The solution is to normalize the initial bearing as shown below
    initial_bearing = math.degrees(initial_bearing)
    compass_bearing = (initial_bearing + 360) % 360

    return compass_bearing

def pather():
    # initialize variables
    gps_loc = update_gps()
    
    # Assuming position is (x, y, z) and orientation is some degree value (0˚ ≤ orientation ≤ 360˚)
    position, orientation = update_pose()
    goal_loc = setttings.gvars.db.get(rediskeys.GPS_GOAL)

    # get the haversine distance between the gps and goal locations
    dist = get_haversine(gps=gps_loc, goal=goal_loc)
    
    #some vector operations to get the final vector here
    bearing = get_bearing(gps_loc, goal_loc)

    return np.multiply(dist, bearing)
