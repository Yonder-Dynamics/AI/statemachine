import signals

print("Importing modules")
from move_to_candidate import move_to_candidate
from waiting import waiting
from pather import pather
from spin import spin

# Make pather return a different signal depending on what node came before it
print("Creating state machine")
st = {
    signals.START: waiting,
    signals.RECIEVED_NEXT: pather,
    signals.ARRIVED: spin,
    signals.CANDIDATE_FOUND: move_to_candidate,
    signals.FOUND: waiting,
}
