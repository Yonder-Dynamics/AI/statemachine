import cv2
import numpy as np
from pyquaternion import Quaternion
import settings

# B G R

upper_bright = np.array([255, 255, 255])
lower_bright = np.array([0, 0, 0])
# Bright blue
upper_bright[0] = 100
lower_bright[0] = 0
# Bright green
upper_bright[1] = 255
lower_bright[1] = 130
# Bright red
upper_bright[2] = 150
lower_bright[2] = 100

TENNIS_BALL_SIZE = .066 #m

cap = cv2.VideoCapture(0)

def get_image():
    _, img = cap.read()
    cv2.imshow("Window", img)
    cv2.waitKey(1)
    return img

# Load YOLO
net = cv2.dnn.readNet(settings.gvars.yolo_weights, settings.gvars.yolo_cfg)
with open(settings.gvars.yolo_classes, 'r') as f:
    classes = [line.strip() for line in f.readlines()]
tennis_ball_id = classes.index(settings.gvars.tennisball_class_id)

outNames = net.getUnconnectedOutLayersNames()
layerNames = net.getLayerNames()
lastLayerId = net.getLayerId(layerNames[-1])
lastLayer = net.getLayer(lastLayerId)

def detect(image):
    w = image.shape[1]
    h = image.shape[0]

    scale = 0.00392
    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(outNames)
    regions = np.concatenate(outs, axis=0)
    scores = regions[:,5:]
    class_ids = np.argmax(scores, axis=1)
    tennis_balls = np.hstack((regions[class_ids == tennis_ball_id, :4], scores[:,class_ids]))
    return tennis_balls

def has_detection(detections, threshold):
    filtered = detections[np.where(detections[:,-1] > threshold)]
    return len(filtered) > 0

def calc_dist(detections, threshold):
    filtered = detections[np.where(detections[:,-1] > threshold)]
    dist = (settings.gvars.tennisball_length * settings.gvars.focal_length) / filtered[:, 2]
    return dist.mean()/1000

def blob_detect(image):
    image = cv2.blur(image, (5,5))
    std = np.std(image)
    mean = np.mean(image)
    #image -= int(mean) + 128

    mask = cv2.inRange(image, lower_bright, upper_bright)# / 255
    cv2.imshow("Mask", mask)
    mask /= 255
    confidence = np.sum(mask)
    char = chr(cv2.waitKey(0) & 0xFF)
    ys = np.expand_dims(np.arange(0, image.shape[0]), 1).dot(np.ones((1, image.shape[1])))
    xs = np.ones((image.shape[0], 1)).dot(np.expand_dims(np.arange(0, image.shape[1]), 0))
    x = (np.sum(np.multiply(ys, mask)) / max(np.count_nonzero(mask), 1))
    y = (np.sum(np.multiply(xs, mask)) / max(np.count_nonzero(mask), 1))
    h = np.std(np.multiply(xs, mask))
    w = np.std(np.multiply(ys, mask))
    # Other libs will return multiple
    return np.array([[x, y, x+h, y+w, confidence]])

# Util function
def draw(image, x, y, r):
    print(x, y, r)
    cv2.circle(image, (int(y), int(x)), int(r), (255,255,255))
    cv2.imshow("Tennis balls", image)
    cv2.waitKey(0)

# Calc orientation to face tennis ball
def calculateOrientation(x, im_width, fov_w):
    angle = float(x-im_width/2.0)/im_width * fov_w
    q = Quaternion(axis=[0,0,1], radians=angle)
    return q, angle

# Calc distance to tennis ball
def calcDist(size, im_width, fov):
    angle = float(size)/im_width * fov_w
    return TENNIS_BALL_SIZE/math.tan(2*angle)/2
