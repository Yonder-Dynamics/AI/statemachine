import signals
import rediskeys 
import math
# from main import globalvars
import sys
import numpy



# Using simple nearest neighbor algorithm for TSP https://www.youtube.com/watch?v=E85l6euMsd0
def tsp(): 
    # Assumes this is updated nodemap, with value at 0 being current location
    nodemap = globalvars.db.get(rediskeys.NODE_MAP)
    # For now, each node will have field position with x, y
    curnode = nodemap[0] # assuming 0 is node cooresponding to current gps location

    bestnode = tsp_iterate(curnode, nodemap)
    
    database = globalvars.db
    database.set(rediskeys.GPS_GOAL, bestnode.get_pos()) 
    database.set(rediskeys.GPS_START, globalvars.db.get(rediskeys.POSE).get_pos())
    database.set(rediskeys.NODE_MAP, nodemap)
    return signals.FOUND_NODE_DESTINATION

# Use to add different heuristics to the TSP algorithm, such as prioritizing
# nodes closer to the center, and nodes where the tennis ball is more likely
# to be.

def tsp_iterate(curnode, nodemap):
    # Switch out this function to test
    bestnode = greedy_tsp(curnode, nodemap)
    if(curnode == bestnode):
        return signals.EXHAUSTED_NODES
    
    nodemap.remove(bestnode)
    nodemap.insert(0, bestnode)
    return bestnode


def greedy_tsp(curnode, nodemap):
    def weight(node1, node2):
        weight = node1.get_distance(node2)
        # weight += get_distance(*node2.get_pos()) * .3 #weight higher for closer to center
        if(node2.probability == 0):
            weight = sys.maxsize
        else:
            weight /= node2.probability # dividing by probability
        return weight

    minimum = sys.maxsize
    bestnode = curnode
    for node in nodemap:
        if(node == curnode):
            continue
        edgeweight = weight(curnode, node)
        if(edgeweight < minimum):
            minimum = edgeweight
            bestnode = node
    
    curnode.mult_probability(.1)

    return bestnode

def get_distance(x, y):
    return math.sqrt(math.pow(x, 2) + math.pow(y, 2))

def weight(node1, node2):
    return math.sqrt(math.pow((node1.position.x - node2.position.x), 2), math.pow((node1.position.y - node2.position.y), 2))
