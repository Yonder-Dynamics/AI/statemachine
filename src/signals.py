import random

_LETTER_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
_SIGLEN = 10

def rand():
    return ''.join(random.choice(_LETTER_SET) for _ in range(_SIGLEN))

RECIEVED_NEXT = rand()
START = rand()
NOT_FOUND = rand()
CANDIDATE_FOUND = rand()
FOUND = rand()
ARRIVED = rand()
FOUND_NODE_DESTINATION = rand()
EXHAUSTED_NODES = rand()
