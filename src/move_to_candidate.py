import signals
import rediskeys
import vision
import cv2
import utils
import math
import settings
import numpy as np
import control

def update_pose():
    # retrieve the pose from redis
    pose = settings.gvars.db.get(rediskeys.POSE)
    return pose.position, utils.getHeading(pose)

def move_to_candidate():
    init_pose = settings.gvars.db.get(rediskeys.POSE)
    # inspect_distance is the max distance we want to inspect, to prevent going forever
    lost_iterations = 0
    while True:
        out = vision.detect(vision.get_image())
        # Check if we're confident ball is found
        # if np.max(out[:, 2]) > settings.gvars.found_threshold:
            # return signals.FOUND

        # Check if we lost the ball
        if np.max(out[:, 4]) < settings.gvars.not_found_threshold:
            lost_iterations += 1
            if lost_iterations > settings.gvars.lost_target_resiliance:
                print("Lost it")
                return signals.NOT_FOUND

        # Refine angle to ball
        # Placeholder
        """
        if distance < settings.gvars.obj_target_dist:
            return signals.FOUND
        ball_pos = 0 # Replace this with ball's image in camera.
        rotation = ball_pos * 2 * math.pi
        pose, orientation = update_pose()
        angle = rotation + orientation
        diffx = settings.gvars.travel_distance * math.cos(angle)
        diffy = settings.gvars.travel_distance * math.sin(angle)
        """
        distance = vision.calc_dist(out, settings.gvars.not_found_threshold) # ask detector for this
        if distance < settings.gvars.obj_target_dist:
            return signals.FOUND
        else:
            # Move straight towards ball # placeholder for actual turning
            print("Moving forward, dist: {}".format(distance))
            cmd = [150]*6
            control.send_drive_cmd(cmd)

        """
        # Setting the pather start and end coordinates
        start = settings.gvars.db.get(rediskeys.POSE).position
        settings.gvars.db.set(rediskeys.GPS_START, start)
        goal = start + [diffx, diffy]
        settings.gvars.db.set(rediskeys.GPS_GOAL, goal)
        """

    return signals.NOT_FOUND
