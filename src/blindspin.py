import signals
import vision
import cv2
import rediskeys
import utils
import math
import settings
import numpy as np
import time
import control

def blindspin():
    for i in range(settings.gvars.turn_attempts):
        # check if there is a ball candidate
        if vision.has_detection(vision.detect(vision.get_image()), settings.gvars.not_found_threshold):
            return signals.CANDIDATE_FOUND
        # actually turn the dame thing
        print("Weeeeeee!")
        speed = 200
        cmd = [speed]*3 + [-speed]*3
        time.sleep(0.2)
        control.send_drive_cmd(cmd)

    return signals.NOT_FOUND

