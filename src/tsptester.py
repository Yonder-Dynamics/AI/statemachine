from tsp import tsp_iterate
from node import Node
import math
import random
import numpy as np

exploration_radius = 10
find_probability = .95
decrease_probability = .2
obstacle_probability = .3
obstacle_detection_radius = 2
distribution_tightness = 10

visited_history = []

def main():
    matrix, nodemap = generate_nodemap(exploration_radius)
    randnode = random.choice(nodemap)
    tennis_ball = randnode.get_pos()
    obstaclemap = generate_obstaclemap(exploration_radius, tennis_ball)
    
    curnode = nodemap[int(len(nodemap) / 2)]
    found = False
    print("tennis ball pos {}".format(tennis_ball))
    while not found:
        visited_history.append(curnode.get_pos())
        print("curr_node {}".format(curnode.get_pos()))
        found = explore(tennis_ball, curnode, find_probability)
        detect_obstacles(obstaclemap, nodemap, curnode.get_pos())
        curnode = tsp_iterate(curnode, nodemap)
    print("found")
    draw_history(nodemap)

def detect_obstacles(obstaclemap, nodemap, pos):
    for xdelta in range(-1 * obstacle_detection_radius, obstacle_detection_radius):
        for ydelta in range(-1 * obstacle_detection_radius, obstacle_detection_radius):
            check = (pos[0] + xdelta, pos[1] + ydelta)
            # This is super shitty. Make nodemap getnode by position.
            for obstacle in obstaclemap:
                if obstacle.get_pos() == check and not obstacle.discovered:
                    for node in nodemap:
                        if node.get_pos() == check:
                            print(node.probability)
                            node.mult_probability(1 - obstacle.probability)
                            obstacle.discovered = True

def remove_node(nodemap, pos):
    for node in nodemap:
        if (node.get_pos()[0] == pos[0]) and (node.get_pos()[1] == pos[1]):
            node.mult_probability(0)


def explore(tennis_ball, cur_node, probability):
    if tennis_ball == cur_node.get_pos():
        if random.random() < probability:
            return True
        else: # Failed exploration
            print("FAILED TO FIND")
            cur_node.mult_probability(decrease_probability)
            return False
    return False
     
def generate_nodemap(radius):
    matrix = np.zeros((radius * 2, radius * 2))
    nodemap = []
    for x in range (-1*radius, radius):
        for y in range (-1*radius, radius):
            if get_distance(x, y) < radius:
                std = distribution_tightness
                node = Node(x, y, matrix, probability=(1/(2 * np.pi * std**2))**.5 * np.exp(-1/(2*std**2)*(get_distance(x, y)**2)))
                nodemap.append(node)
                matrix[x,y] = node.probability
    for node in nodemap:
        node.mass_add_connections(nodemap)
    return matrix, nodemap

def generate_obstaclemap(radius, tennis_ball):
    matrix = np.zeros((radius * 2, radius * 2))
    obstaclemap = []
    for x in range (-1*radius, radius):
        for y in range (-1*radius, radius):
            if get_distance(x, y) < radius:
                node = Node(x, y, matrix, random.random()**3)
                if tennis_ball == (x, y):
                    node.mult_probability(0)
                obstaclemap.append(node)
    return obstaclemap

def get_distance(x, y):
    return math.sqrt(math.pow(x, 2) + math.pow(y, 2))

def draw_history(nodemap):
    import matplotlib.pyplot as plt
    import networkx as nx

    G=nx.DiGraph()
    graph_locations = {each.get_pos():i for i, each in enumerate(nodemap)}

    for i, each in enumerate(nodemap):
        G.add_node(i,pos=each.get_pos())
    
    previous = visited_history[0]
    for each in visited_history[1:]:
        pre_index = graph_locations[previous]
        curr_index = graph_locations[each]
        G.add_edge(pre_index, curr_index)
        previous = each

    pos=nx.get_node_attributes(G,'pos')
    nx.draw(G,pos, node_size=[node.probability * 1000 for i, node in enumerate(nodemap)], with_labels=False)
    plt.show()


if __name__ == "__main__": main()
