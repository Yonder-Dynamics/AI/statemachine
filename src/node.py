class Node():
    def __init__(self, x_pos, y_pos, matrix_ref, probability=1.0, connected_nodes = None):
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.probability = probability
        self.connections = []
        self.discovered = False # For obstacles
        self.matrix_ref = matrix_ref
        if(connected_nodes is not None):
            for each in connected_nodes:
                self.connections.append(each)

    def get_pos(self):
        return (self.x_pos, self.y_pos)
    
    def mult_probability(self, mult):
        self.probability *= mult
        self.matrix_ref[self.x_pos,self.y_pos] = self.probability
    
    def get_connections(self):
        return self.connections

    def add_connection(self, node):
        self.connections.append(node)
        
    def update_connections(self):
        for connection in self.connections:
            connection = self.mult_probability(.8)


    def mass_add_connections(self, connection_list):
        for each in connection_list:
            if(each != self):
                self.connections.append(each)
    
    def remove_connection(self, node):
        if(node in self.connections):
            self.connections.remove(node)
        self.connections.append(node)
        
    def mass_remove_connections(self, connection_list):
        for each in connection_list:
            if(each in self.connections):
                self.connections.remove(each)

    def get_distance(self, other_node):
        return ((self.x_pos - other_node.x_pos) ** 2 +  (self.y_pos - other_node.y_pos) ** 2) ** .5
