import signals
import redis
import argparse
import yaml
import settings

parser = argparse.ArgumentParser()
parser.add_argument("--redis_host", default="localhost")
parser.add_argument("--redis_port", default=6379, type=int)
parser.add_argument("--turn_iterations", default=1000, help="number of times the rover attempts to turn in place", type=int)
parser.add_argument("--config", default=None, help="Config file for rover", type=str)
parser.add_argument("--yolo_cfg", default=None, help="Config file for yolo net", type=str)
parser.add_argument("--yolo_weights", default=None, help="Weight file for yolo net", type=str)
parser.add_argument("--yolo_classes", default=None, help="Classes file for yolo net", type=str)
parser.add_argument("--eval_threshold", default=0.5, help="Yolo eval threshold for tennis ball", type=float)
parser.add_argument("--turn_attempts", default=10000, help="Amount of times the rover spins in place. Look at the spin to see how long that converts to", type=int)

def parse_args(parser):
    args = parser.parse_args()
    if args.config is not None:
        with open(args.config, "r") as f:
            data = yaml.load(f)
            delattr(args, 'config')
            arg_dict = args.__dict__
            for key, value in data.items():
                if isinstance(value, list):
                    for v in value:
                        arg_dict[key].append(v)
                else:
                    arg_dict[key] = value
    return args

settings.init(parse_args(parser))
settings.gvars.db = redis.StrictRedis(host=settings.gvars.redis_host, port=settings.gvars.redis_port)
from test_machine import st

exit = signals.RECIEVED_NEXT
sig = signals.START

print("Starting state machine")
while sig != exit:
    sig = st[sig]()
    print(sig)
