#############################################
# Object detection - YOLO - OpenCV
# Author : Arun Ponnusamy   (July 16, 2018)
# Website : http://www.arunponnusamy.com
############################################


import cv2
import argparse
import numpy as np
import settings

# ap = argparse.ArgumentParser()
# ap.add_argument('-i', '--image', required=True,
#                 help = 'path to input image')
# ap.add_argument('-c', '--config', required=True,
#                 help = 'path to yolo config file')
# ap.add_argument('-w', '--weights', required=True,
#                 help = 'path to yolo pre-trained weights')
# ap.add_argument('-cl', '--classes', required=True,
#                 help = 'path to text file containing class names')
# args = ap.parse_args()

    classes = None

    with open(netclasses, 'r') as f:
        classes = [line.strip() for line in f.readlines()]

net = cv2.dnn.readNet(weights, config)

def get_output_layers(net):
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    return output_layers

def draw_prediction(img, class_id, confidence, x, y, x_plus_w, y_plus_h):
    label = str(classes[class_id])
    color = COLORS[class_id]
    cv2.rectangle(img, (x,y), (x_plus_w,y_plus_h), color, 2)
    cv2.putText(img, label, (x-10,y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)
    

def get_prediction(image, config, weights, netclasses):

    Width = image.shape[1]
    Height = image.shape[0]
    scale = 0.00392

    COLORS = np.random.uniform(0, 255, size=(len(classes), 3))

    blob = cv2.dnn.blobFromImage(image, scale, (416,416), (0,0,0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(get_output_layers(net))

    class_ids = []
    confidences = []
    boxes = []
    conf_threshold = 0.5
    nms_threshold = 0.4


    print(outs)
    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                center_x = int(detection[0] * Width)
                center_y = int(detection[1] * Height)
                w = int(detection[2] * Width)
                h = int(detection[3] * Height)
                x = center_x - w / 2
                y = center_y - h / 2
                class_ids.append(class_id)
                confidences.append(float(confidence))
                boxes.append([x, y, w, h])


    indices = cv2.dnn.NMSBoxes(boxes, confidences, conf_threshold, nms_threshold)

    # for i in indices:
    #     i = i[0]
    #     box = boxes[i]
    #     x = box[0]
    #     y = box[1]
    #     w = box[2]
    #     h = box[3]
    #     draw_prediction(image, class_ids[i], confidences[i], round(x), round(y), round(x+w), round(y+h))
    for i in range(0, len(class_ids)):
        if class_ids[i] == settings.gvars.tennisball_class_id:
            box = boxes[i]
            percent_width = ((boxes[2] + boxes[3])/2) / Width
            dist = (settings.gvars.tennisball_length * settings.gvars.focal_length) / percent_width
            return np.array([box[0], box[1], confidences[i], box[2], box[3], dist])
    return np.array([])

