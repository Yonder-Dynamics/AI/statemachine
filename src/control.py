import settings
import json
import rediskeys

def send_drive_cmd(speeds):
    cmd = {
            "type": "drive",
            "data": speeds,
    }
    settings.gvars.db.publish(rediskeys.DRIVE, json.dumps(cmd))
