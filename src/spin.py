import signals
import vision
import cv2
import rediskeys
import utils
import math
import settings
import numpy as np
import time

def spin():
    init_pose = settings.gvars.db.get(rediskeys.POSE)
    while init_pose is None:
        print("Did not recieve pose")
        init_pose = settings.gvars.db.get(rediskeys.POSE)
        time.sleep(1)
    last_heading = utils.getHeading(init_pose)
    amt_turned = 0
    for i in range(settings.gvars.turn_attempts):
        out = vision.detect(vision.get_image())
        if np.max(out[:, 4]) > settings.gvars.eval_threshold:
            return signals.FOUND
        # actually turn the dam thing
        speed = utils.scale_speed(amt_turned, 2*math.pi)
        cmd = [speed]*3 + [-speed]*3
        control.send_drive_cmd(cmd)

        # check if we have turned all the way around
        pose = settings.gvars.db.get(rediskeys.POSE)
        diff = abs(last_heading - utils.getHeading(pose))
        amt_turned += diff
        if amt_turned > 2*math.pi:
            break
    return signals.NOT_FOUND
